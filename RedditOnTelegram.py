#!/usr/bin/python3
# |==============================================|
# |             RedditOnTelegram Bot             |
# |        Python bot designed for simple        |
# |    autoforwarding from Reddit to Telegram    |
# |==============================================|
# |  Created by gitlab.com/octospacc | License:  |
# |   http://gnu.org/licenses/agpl-3.0.en.html   |
# |==============================================|

import os, re, time, json
from requests import get, post
from bs4 import BeautifulSoup

# All user configs go here.
refresh      = 5  # How often (in minutes) to refresh the feed and forward posts to Telegram. Default is 5.
user_agent   = "RedditOnTelegram Bot <development>" # User agent to use when sending requests. It is good practice to set it in a way that explain why you are sending the requests.
#post_max     = 0 # Max number of new posts to scrape from a subreddit at once. 0 is unlimited.
bot_username = "RedditOnTelegramBot" # Username of the worker Telegram bot. It will appear in message captions and other sections.
chats        = [] # Telegram Chat IDs to forward to go here, inside quotes and separated with commas (example: [123456789, -1234567890123]).
subs_top     = [] # Names of the subreddits to scrape posts from;
subs_new     = ["RedditOnTelegramTest"] # They must be put in the appropriate list (top, new, hot, rising), depending on the type of listing from which to scrape posts;
subs_hot     = [] # The same subreddit name can be put in more than a single list at once, but that will result in duplicate posts.
subs_rising  = [] # The subreddit names must be put inside quotes and separated with commas (example: ["cutelittlefangs", "goodanimemes"]).

posts, post_queue = [], []
datadir = "ROT_DATA/"
tempdir = datadir + "temp/"
postedlogpath = datadir + "posted.log"
tokenfile = "bot_token.txt"
redsubpath = "https://old.reddit.com/r/"
imgpath = "https://i.redd.it/"
extpath = "https://external-preview.redd.it/"

# Fetches the JSON feed from each subreddit in the given list, saves as file.
def jsonfetch(subcat):
	if subcat == subs_top:
		subcatname = "top.json"
	elif subcat == subs_new:
		subcatname = "new.json"
	elif subcat == subs_hot:
		subcatname = "hot.json"
	elif subcat == subs_rising:
		subcatname = "rising.json"
	else:
		print("Something went wrong in feedrefresh(" + str(subcat) + ", stopping function")
		return
	
	if len(subcat) != 0:
		for i in subcat:
			try:
				jsonfile = open(tempdir + i + "_" + subcatname, "w")
				jsonfile.write(get(redsubpath + i + "/" + subcatname, headers={"User-agent": user_agent}).content.decode("utf-8"))
				jsonfile.close()
				print("Fetched JSON feed of " + i + "_" + subcatname + "... At " + time.ctime())
			except:
				print("Rip, error fetching JSON feed of " + i + "_" + subcatname + "... At " + time.ctime())

# Parses the downloaded JSON files to keep only needed information.
def jsonparse(subcat):
	if subcat == subs_top:
		subcatname = "top.json"
	elif subcat == subs_new:
		subcatname = "new.json"
	elif subcat == subs_hot:
		subcatname = "hot.json"
	elif subcat == subs_rising:
		subcatname = "rising.json"
	
	# subreddit, title, author, id, created, is_video, is_gallery, 
	posts_list = []
	
	if len(subcat) != 0:
		for i in subcat:
			jsonfile = open(tempdir + i + "_" + subcatname, "r")
			jsonstring = json.loads(jsonfile.read()).get("data")["children"]
			jsonfile.close()
			
			postedlogfile = open(datadir + "posted.log", "r")
			postedlogstring = postedlogfile.read()
			postedlogfile.close()
			
			post_n, post_dict = 0, {}
			
			for j in jsonstring:
				post_base = jsonstring[post_n]["data"]
				posts_list += [{"subreddit":post_base["subreddit"], "author":post_base["author"], "title":post_base["title"], "created":post_base["created"], "id":post_base["id"]}]
				post_n += 1
			
			print(posts_list)

def feedrefresh(subcat):
	# Gets the rss file from each subreddit in the given list
	if subcat == subs_top:
		subcatname = "top.rss"
	elif subcat == subs_new:
		subcatname = "new.rss"
	elif subcat == subs_hot:
		subcatname = "hot.rss"
	elif subcat == subs_rising:
		subcatname = "rising.rss"
	else:
		print("Something went wrong in feedrefresh(" + str(subcat) + ", stopping function")
		return

	if len(subcat) != 0:
		for i in subcat:
			try:
				rssfile = open(tempdir + i + "_" + subcatname, "w")
				rssfile.write(get(redsubpath + i + "/" + subcatname, headers={"User-agent": user_agent}).content.decode("utf-8"))
				rssfile.close()
				print("Fetched RSS feed of " + i + "_" + subcatname + "... At " + time.ctime())
			except:
				print("Rip, error fetching RSS feed of " + i + "_" + subcatname + "... At " + time.ctime())

def feedparse(subcat, posts_):
	# It takes the posts list and the subs list, it parses every new post in the sub rss file
	# And then release the posts list in which every item is a list with subredditname,username,postid,postname,title,link

	if subcat == subs_top:
		subcatname = "top.rss"
	elif subcat == subs_new:
		subcatname = "new.rss"
	elif subcat == subs_hot:
		subcatname = "hot.rss"
	elif subcat == subs_rising:
		subcatname = "rising.rss"
	else:
		print("Something went wrong in feedparse(" + str(subcat) + ", stopping function")
		return

	if len(subcat) != 0:
		for i in subcat:
			rssfile = open(tempdir + i + "_" + subcatname, "r")
			rssfeed = rssfile.read()
			rssfile.close()
			posts_ = []
			posttocheck = re.findall("<entry>.*?</entry>", rssfeed)
			for l in posttocheck:
				fanculo = re.findall("<content.*</content>", l)[0]
				l = l.replace(fanculo, "")
				subname = i
				author = re.findall("<name>.*</name>", l)[0].replace("/u/","")
				author = author.replace(author[:6], "").replace(author[len(author)-7:], "")
				link = re.findall("<link href=\".*\" /><upd", l)[0]
				link = link.replace(link[:12], "").replace(link[len(link)-8:], "")
				title = re.findall("<title>.*</title>", l)[0]
				title = title.replace(title[:7], "").replace(title[len(title)-8:], "")
				postid = re.findall("https://old\.reddit\.com/r/" + i + "/comments/.*/", link)[0]
				postid = postid.replace("https://old.reddit.com/r/" + i + "/comments/", "")
				postid = postid.replace(postid[postid.find("/"):], "")
				postname = re.findall("https://old\.reddit\.com/r/" + i + "/comments/" + postid + "/.*/", link)[0]
				postname = postname.replace("https://old.reddit.com/r/" + i + "/comments/" + postid + "/", "")
				postname = postname.replace(postname[len(postname)-1:], "")
				postlist = [subname, author, postid, postname, title, link]
				posts_.append(postlist)
	return posts_


def unduplicate(posts_, post_queue_):
	postedlogfile = open(postedlogpath, "r")
	postedlog = postedlogfile.read()
	postedlogfile.close()

	for i in posts_:
		if not i[2] in postedlog:
			post_queue_ += [i]
	posts_ = []
	return posts_, post_queue_


def contentscrape(post_queue_):
	for i in post_queue_:
		curr_char, curr_char2, curr_prop_start, curr_prop_end, exturls, text = 0, 0, 0, 0, "", ""
		
		try:
			#htmlfile = open(tempdir + "temp.html" , "w")
			#htmlfile.write((get(redsubpath + i[0] + "/comments/" + i[2] + i[3], headers={"User-agent":user_agent}).content).decode("utf-8"))
			#htmlfile.close()
			htmldl = (get(redsubpath + i[0] + "/comments/" + i[2] + i[3], headers={"User-agent":user_agent}).content).decode("utf-8")
			print("Fetched HTML page of " + i[2] + "... At " + time.ctime())
		except:
			print("Rip, error fetching HTML page of " + i[2] + "... At " + time.ctime())
			return
		
		for j in range(len(htmldl)):
			curr_char += 1
			
			if htmldl[j:j+16] == "{\"target_url\": \"":
				curr_prop_start = curr_char + 15
			
			if htmldl[j:j+16] == "\", \"target_id\": ":
				curr_prop_end = curr_char - 1
				l4compare = htmldl[curr_prop_end-4:curr_prop_end]
				l5compare = htmldl[curr_prop_end-5:curr_prop_end]
				
				if l4compare == ".jpg" or l4compare == ".png" or l4compare == ".bmp" or l4compare == ".svg" or l4compare == ".avi" or l4compare == ".mov" or l4compare == ".mp4" or l4compare == ".gif":
					print(htmldl[curr_prop_end-4:curr_prop_end])
				
				elif l5compare == ".webp" or l5compare == ".webm":
					print(htmldl[curr_prop_end-5:curr_prop_end])
				
				elif htmldl[curr_prop_end-25:curr_prop_end] == "reddit.com/gallery/" + i[2]:
					print("EPICO GALLERY")
					print(htmldl[curr_prop_end-25:curr_prop_end])
					"""for k in range(len(htmldl)):
						tmpvar = 0""" #if htmldl
					"""
					<div class="md"><p>
					<div class="md"><h1>
					</div>"""

				"""if hasgallery:
					if htmldl[j:j+45] == "<div class=\"media-preview-content\"><a href=\"":
						curr_prop_start = curr_char + 45
						print(htmldl[curr_prop_start:curr_prop_start+50])
						tmpvar = input()
					
					if htmldl[j:j+37] == "\" class=\"may-blank\" target=\"_blank\">":
						curr_prop_end = curr_char -30
						print(htmldl[curr_prop_start:curr_prop_end])
						tmpvar = input()"""
	return post_queue_


def main():
	posts, post_queue = [], []
	feedrefresh(subs_top)
	feedrefresh(subs_new)
	feedrefresh(subs_hot)
	feedrefresh(subs_rising)
	
	jsonfetch(subs_top)
	jsonfetch(subs_new)
	jsonfetch(subs_hot)
	jsonfetch(subs_rising)
	
	jsonparse(subs_top)
	jsonparse(subs_new)
	jsonparse(subs_hot)
	jsonparse(subs_rising)

	posts = feedparse(subs_top, posts)
	posts = feedparse(subs_new, posts)
	posts = feedparse(subs_hot, posts)
	posts = feedparse(subs_rising, posts)

	posts, post_queue = unduplicate(posts, post_queue)
	post_queue = contentscrape(post_queue)

	#tokenfile = open("ROT_DATA/bot_token.txt", "r")
	#token = tokenfile.read()
	#tokenfile.close()

	for mela in post_queue:
		link = mela[5]
		page = get(link, headers={"User-agent": user_agent})
		zuppina = BeautifulSoup(page, "lxml")

		'''
		if mela[2] in open("ROT_DATA/posted.log", "r").read():
			pass
		else:
			message = {
				"chat_id": 158163241,
				"text": "O fra si nu pircuoco.\n\n" +
						"U Subreddit: " + mela[0] +
						"\nO Scrittore: " + mela[1] +
						"\nO postid: " + mela[2] +
						"\nO link alo post: " + mela[3] +
						"\nO titolo ro post: " + mela[4]
			}
			f = post("https://api.telegram.org/bot" + token +
					 "/sendMessage?chat_id=" + str(message["chat_id"]) +
					 "&text=" + message["text"])
			with open("ROT_DATA/posted.log", "a") as posted:
				posted.write(mela[2])
			'''
	print("Cycle completed. Next refresh in " + str(refresh) + " minutes.")
	time.sleep(refresh * 60)
	main()


if __name__ == "__main__":
	print("Starting RedditOnTelegram Free/Libre bot...\nRefresh time is set to " + str(refresh) + " minutes.")

	if not os.path.exists(datadir):
		try:
			os.mkdir(datadir)
		except:
			print("Rip, error creating " + datadir + " directory... At " + time.ctime())

	if not os.path.exists(tempdir):
		try:
			os.mkdir(tempdir)
		except:
			print("Rip, error creating " + tempdir + " directory... At " + time.ctime())

	if not os.path.exists(postedlogpath):
		try:
			postedlogfile = open(postedlogpath, "w")
			postedlogfile.close()
		except:
			print("Rip, error creating " + postedlogpath + " file... At " + time.ctime())

	if not os.path.exists(datadir + tokenfile):
		print("Token file not found! Please create a file named " + tokenfile + " in the " + datadir + " folder, and write the Telegram bot token inside.")
		#exit()
	main()

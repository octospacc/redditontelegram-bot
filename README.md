# RedditOnTelegram Bot

Python bot designed to fetch posts and their content from user defined subreddits from Reddit, and forward everything to a Telegram chat, all of this automatically, without any user interaction needed apart from the initial configuration.